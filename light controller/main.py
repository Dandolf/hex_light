import machine
import utime
import network
import urequests
import ujson

import dht
import neopixel

# Peripherals
pixels = neopixel.NeoPixel(machine.Pin(12), 72, bpp=4)
tempSensor = dht.DHT22(machine.Pin(4))
motionSensor = machine.Pin(5, machine.Pin.IN)
button = machine.Pin(2, machine.Pin.IN, machine.Pin.PULL_UP)

# Program variables
currentTemp = 0
currentHumidity = 0
motionDetected = False
buttonPressed = False
currentLight = (0,0,0,0)
manualLightOn = False
manualLightPrevious = False

# Configure WiFi
ssid = ''
password = ''
host = ''
port = 8080

def do_connect():
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        pixels[70] = (0, 0, 30, 0)
        pixels[68] = (0, 0, 30, 0)
        pixels[66] = (0, 0, 30, 0)
        pixels.write()
        sta_if.active(True)
        sta_if.connect(ssid, password)
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())

do_connect()

pixels[66] = (0, 0, 0, 0)
pixels[68] = (0, 0, 0, 0)
pixels[70] = (0, 0, 0, 0)
pixels.write()

def readTemp():
  global currentTemp, currentHumidity
  tempSensor.measure()
  currentTemp = tempSensor.temperature()
  currentHumidity = tempSensor.humidity()
  #print(tempSensor.temperature())

def detectMotion():
  global motionDetected
  if motionSensor.value() == 1:
      motionDetected = True

def updatePixels(r,g,b,w):
  for i in range(72):
    pixels[i] = (r,g,b,w)
  pixels.write()

sensorScanTick = utime.ticks_ms()
serverPingTick = utime.ticks_ms()
sensorUpdateTick = utime.ticks_ms()

buttonDebounceTick = utime.ticks_ms()
runningDebounce = False

while True: 

  # Every 2 seconds read sensors
  if utime.ticks_diff(utime.ticks_ms(), sensorScanTick) >= 2000:
    readTemp()
    detectMotion()
    print(currentTemp)    
    print(motionDetected)
    sensorScanTick = utime.ticks_ms()
  
  # Every 60 seconds ping server for light status
  if utime.ticks_diff(utime.ticks_ms(), serverPingTick) >= 60000:
    response = urequests.get('http://' + host + ':' + str(port) + '/getLightStatus')
    print(response.json())
    color = response.json()['color']
    if not manualLightOn:
      updatePixels(color[0], color[1], color[2], color[3])
    serverPingTick = utime.ticks_ms()

  # Every 5 minutes update server with current sensor data
  if utime.ticks_diff(utime.ticks_ms(), sensorUpdateTick) >=  300000:
    dataPacket = ujson.dumps({'temp': currentTemp, 'humidity': currentHumidity, 'motion': motionDetected})
    response = urequests.post('http://' + host + ':' + str(port) + '/sensorDataUpdate', headers = {'content-type': 'application/json'}, data = dataPacket)
    print(response.json())
    motionDetected = False
    sensorUpdateTick = utime.ticks_ms()

  # Check if button is pressed and start debouncing 
  if button.value() == 0:
    if not runningDebounce:
      runningDebounce = True
      buttonDebounceTick = utime.ticks_ms()
    elif utime.ticks_diff(utime.ticks_ms(), buttonDebounceTick) >= 50:
      # Debouncing complete
      # Toggle manual light setting
      if not buttonPressed:
        manualLightOn = not manualLightOn
        runningDebounce = False
      buttonPressed = True
  else:
    runningDebounce = False
    buttonPressed = False

  if manualLightOn != manualLightPrevious:
    if manualLightOn:
      updatePixels(0,0,0,20)
    else:
      updatePixels(0,0,0,0)
    manualLightPrevious = manualLightOn
  
  utime.sleep_ms(2)
