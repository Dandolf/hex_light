const express = require('express')
const fs = require('fs')

const app = express()

app.use(express.json())

var lights = {
    on: false,
    mode: 'standard',
    color: [0, 0, 0, 0]
}

var schedule = [
    {
        startHour: 20,
        startMinute: 10,
        endHour: 20,
        endMinute: 25,
        color: [0, 0, 100, 0]
    },
    {
        startHour: 20,
        startMinute: 26,
        endHour: 20,
        endMinute: 30,
        color: [0, 0, 0, 100]
    },
    {
        startHour: 21,
        startMinute: 10,
        endHour: 21,
        endMinute: 12,
        color: [255, 255, 255, 0]
    },
    {
        startHour: 21,
        startMinute: 13,
        endHour: 21,
        endMinute: 14,
        color: [0, 255, 0, 0]
    },
]

// Accepts sensor data and sends back light data
app.post('/sensorDataUpdate', (req, res) => {
    // Check if file for today exists
    const today = new Date
    const todayDate = (today.getMonth() + 1) + '-' + today.getDate() + '-' + today.getFullYear()
    const dataFileName = './sensor data/' + todayDate + '.json'

    var todayData = []
    if (fs.existsSync(dataFileName)) {
        todayData = JSON.parse(fs.readFileSync(dataFileName))
    }

    console.log(req.body)

    todayData.push({
        time: Date.now(),
        temp: req.body.temp,
        humidity: req.body.humidity,
        motion: req.body.motion
    })

    fs.writeFileSync(dataFileName, JSON.stringify(todayData))

    checkSchedule()
    console.log(lights)
    res.send(lights)
})

app.get('/getLightStatus', (req, res) => {
    checkSchedule()
    console.log(lights)
    res.send(lights)
})

app.get('/lightOn', (req, res) => {
    lights.on = true
    res.send({success: true})
})

app.get('/lightOff', (req, res) => {
    lights.on = false
    res.send({success: true})
})

app.listen(8080, () => {
    console.log('Server running on port 8080')
})

function checkSchedule () {
    const currentTime = new Date
    const currentHour = currentTime.getHours()
    const currentMinutes = currentTime.getMinutes()

    var currentTimeBlock = schedule.find(timeBlock => {
        return ((timeBlock.startHour < currentHour 
                || (timeBlock.startHour == currentHour && timeBlock.startMinute <= currentMinutes)
            ) && (timeBlock.endHour > currentHour 
                || (timeBlock.endHour == currentHour && timeBlock.endMinute >= currentMinutes)
            )
        )
    })

    if (!!currentTimeBlock) {
        lights.on = true
        lights.color = currentTimeBlock.color
    } else {
        lights.on = false
        lights.color = [0, 0, 0, 0]
    }
}